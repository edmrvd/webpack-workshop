import _ from 'lodash';
// import './scss/styles.scss';
console.log("Labas pasauli");

console.log([1,2,3].map(item => item*2), ' es6');
console.log(_.multiply(2, 2), ' lodash');


function component() {
    let element = document.createElement('div');
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    return element;
}

document.body.appendChild(component());
